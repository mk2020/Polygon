package Servlette;

import Util.DomainFacade;
import Util.Facade;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author LoD
 */
@WebServlet(name = "Loginhandle", urlPatterns = {"/Loginhandle"})
public class Loginhandle extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true); //det lave session object fordi getSession(true) vis det var false vil den ikke gøre det
        String origin = request.getParameter("origin");
        Facade Fa = new Facade(); //laver adgang til DBFavade
        DomainFacade DF = new DomainFacade();
        switch (origin) 
        {
            case "login":
            {
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                int[] UI = (DF.validate(username, password));
                if (UI[0] >= 1 && UI[1] >= 1)
                {
                    session.setAttribute("Username", username);
                    session.setAttribute("Password", password);
                    session.setAttribute("usertype", UI[0]);
                    session.setAttribute("userID", UI[1]);
                    switch ((int)session.getAttribute("usertype")) 
                    {
                        case 1:
                            response.sendRedirect("Normaluser.jsp");
                            break;
                        case 2:
                            response.sendRedirect("Normaluser.jsp");
                            break;
                        default:
                            response.sendRedirect("Normaluser.jsp");
                            break;
                    }
                }
                else 
                {
                    response.sendRedirect("ikkeloggetind.jsp"); // det give den rigtigt URL vider og ikke loginchecker
                    //request.getRequestDispatcher("ikkeloggetind.jsp").forward(request, response); //den give ikke URL vider
                }
                break;
            }
            case "backtostart":
            {
                switch ((int)session.getAttribute("usertype")) 
                {
                    case 1:
                        response.sendRedirect("Normaluser.jsp");
                        break;
                    case 2:
                        response.sendRedirect("Normaluser.jsp");
                        break;
                    default:
                        response.sendRedirect("Normaluser.jsp");
                        break;
                }
                break;
            }
            case "logout":
            {
                session.invalidate();
                response.sendRedirect("index.html");
                break;
            }
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);}
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}