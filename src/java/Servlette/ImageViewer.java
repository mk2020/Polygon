/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlette;

import Mapper.ImageMapper;
import Util.DomainFacade;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Bruger
 */
@WebServlet(name = "ImageViewer", urlPatterns = {"/ImageViewer"})
public class ImageViewer extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
      
        try (PrintWriter out = response.getWriter()) {
           
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection con = null;
        DomainFacade f = new DomainFacade();
        int builidingId = Integer.parseInt(request.getParameter("buildingID"));
       // String builidingPic = request.getParameter("builidingPic");
        //byte[] content = im.showImagesByID(builidingPic);
        byte[] content = f.showImage(builidingId);
        
          response.setContentLength(content.length);
          response.getOutputStream().write(content); 
       
        
       // request.getRequestDispatcher("result.jsp").forward(request, response);
        //request.setAttribute("images", builidingPic);
       
        

    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
