/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlette;

import Entity.Building;
import Mapper.BuildingMapper;
import Mapper.ImageMapper;
import Util.DomainFacade;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Util.Facade;

/**
 *
 * @author Bruger
 */
@WebServlet(name = "ImageDelete", urlPatterns = {"/ImageDelete"})
public class ImageDelete extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DomainFacade f = new DomainFacade();
        
        response.setContentType("text/html");
       
       //HttpSession session = request.getSession(true);
        //String name = request.getParameter("imageName");
        
       // String name = (String) session.getAttribute("imageName");
        //System.out.println("HERE IS THIIIISSS:"+name);
        int id = Integer.parseInt(request.getParameter("Id"));
        String buildingAdress = f.getBu_ID(id);
        f.deleteImage(id);
//        request.setAttribute("Message", message);
       response.sendRedirect("result.jsp?buildinpPic="+buildingAdress);
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
