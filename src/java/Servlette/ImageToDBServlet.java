package Servlette;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import DB.DBC;
import Mapper.ImageMapper;
import Util.DomainFacade;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author mahnaz
 */
@WebServlet(urlPatterns = {"/ImageToDBServlet"})
@MultipartConfig
public class ImageToDBServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Connection con = DBC.getCon();
        DomainFacade f = new DomainFacade();
                
        String message = "";
        String description = "";
        int buID =0;
        description = request.getParameter("description"); // Retrieves <input type="text" name="description">
        Part filePart = request.getPart("file"); // Retrieves <input type="file" name="file">
        //String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
        buID = Integer.parseInt(request.getParameter("id"));
        InputStream fileContent = filePart.getInputStream();
        
        boolean saved = f.saveImage(buID,description, fileContent);

        if (saved) {
            message = "File uploaded and saved into database";
        } else {
            message = "Something went wrong when saving the image";
        }

        request.setAttribute("Message", message);

        //request.setAttribute("Description", description);
        getServletContext().getRequestDispatcher("/upload.jsp").forward(request, response);

//            getServletContext().getRequestDispatcher("/retrieve.jsp").forward(request, response);
    }
}
