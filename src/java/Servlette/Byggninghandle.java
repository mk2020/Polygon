package Servlette;

import Util.Facade;
import Entity.Building;
import Mapper.ImageMapper;
import Util.DomainFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LoD
 */
@WebServlet(name = "Byggninghandle", urlPatterns = {"/Byggninghandle"})
public class Byggninghandle extends HttpServlet 
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter())
        {
            out.print("ERROR");
            String origin = request.getParameter("type");
            Facade Fa = new Facade();
            DomainFacade DF = new DomainFacade();
            ImageMapper im = new ImageMapper();
            ArrayList<Building> listofbuilding = new ArrayList();
            int UT = (int)request.getSession().getAttribute("usertype");
            if (UT == 1)    {listofbuilding = DF.listofbuilding();                                                      }
            else            {listofbuilding = DF.getUsersBuilding((int) request.getSession().getAttribute("userID"));   }
            request.getSession().setAttribute("buildingList", listofbuilding);
            switch (origin) 
            {
                case "listBuildings":
                {   
                    response.sendRedirect("Byninglist.jsp");
                    break;
                }
                case "getbuilding":
                {   
                    String selectBuildingSF = request.getParameter("SELBUI");
                    int selectBuilding = Integer.parseInt(selectBuildingSF);
                    Building SLCBUD = DF.Build(selectBuilding);
                    request.getSession().setAttribute("SelectBuilding", SLCBUD);
                    response.sendRedirect("Edit_Building.jsp");
                    break;
                }
                case "add":
                {   
                    Building bean = new Building();
                    bean.setUsID((int)request.getSession().getAttribute("userID"));
                    bean.setAdresse(request.getParameter("address"));
                    bean.setZipcode(request.getParameter("zip_code"));
                    bean.setCity(request.getParameter("city"));
                    bean.setHousenumber(request.getParameter("b_number"));
                    bean.setFloor(request.getParameter("floor"));
                    bean.setSize(request.getParameter("size"));
                    bean.setRoomamount(request.getParameter("number_room"));
                    bean.setRoomnumber(request.getParameter("room_number"));
                    DF.addBuilding(bean);
                    response.sendRedirect("Add_building.jsp");
                    break;
                }
                case "editBuilding":
                {   
                    Building bean = new Building();
                    String BID = request.getParameter("BuildingID");
                    int BiD = Integer.parseInt(BID);
                    bean.setBuID(BiD);
                    bean.setAdresse(request.getParameter("address"));
                    bean.setZipcode(request.getParameter("zip_code"));
                    bean.setCity(request.getParameter("city"));
                    bean.setHousenumber(request.getParameter("b_number"));
                    bean.setFloor(request.getParameter("floor"));
                    bean.setSize(request.getParameter("size"));
                    bean.setRoomamount(request.getParameter("number_room"));
                    bean.setRoomnumber(request.getParameter("room_number"));
                    DF.editBuilding(bean);
                    response.sendRedirect("Normaluser.jsp");
                    break;
                }
                case "delete":
                {   
                    String selectBuildingSF = request.getParameter("SELBUI");
                    int selectBuilding = Integer.parseInt(selectBuildingSF);
                    Building SLCBUD = DF.Build(selectBuilding);
                    int buildID =SLCBUD.getBuID();
                   
                    im.AllImageForThisBuilding(buildID);
                    DF.deleteBuilding(selectBuilding);
                    response.sendRedirect("Normaluser.jsp");
                    break;
                }
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}