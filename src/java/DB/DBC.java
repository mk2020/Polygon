package DB;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author LoD
 */
public class DBC 
{
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String URL    = "jdbc:mysql://138.68.108.207:3306/Polygon"; //hvad type database det er så placing i verden/localed
    private static final String ID     = "polyadmin"; // mysql usernavn
    private static final String PW     = "ikketestmentest"; //mysql password 
    
    public static Connection getCon()
    {
        try
        {
            Class.forName(driver);
            con = DriverManager.getConnection(URL, ID, PW);
        }
        catch (SQLException ex)  
        {
            System.out.println(ex+" :Error; SQLException kun ikke for getConnection i DBC getCon");
        } 
        catch (ClassNotFoundException ex) 
        {
            System.out.println(ex + " :Error: ClassNotFoundExection i DBC getCon");
        }
        return con;
    }
    private static Connection con;
}
