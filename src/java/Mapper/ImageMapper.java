/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mapper;

import Entity.Image;
import DB.DBC;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mahnaz
 */
public class ImageMapper {

    Connection con = DBC.getCon();
    private InputStream photo;

    public ArrayList<Image> getAllImagesDescription() {
        ArrayList<Image> images = new ArrayList();
        try {

            PreparedStatement pstmt = con.prepareStatement("SELECT PIm_ID, description FROM image");
            
            ResultSet rs = pstmt.executeQuery();
            //Statement pstmt = con.createStatement();
           // ResultSet rs = pstmt.executeQuery("SELECT photo, PIm_ID, description FROM image");
            while (rs.next()) {
                
                int id = rs.getInt("PIm_ID");
                String name = rs.getString("description");
                images.add(new Image(id, name,null));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
           
        return images;
    }

    public boolean saveImage(int id, String name, InputStream file) {
        PreparedStatement ps;
        try {
            ps = con.prepareStatement("insert into image (FBu_ID, description, photo) values(?,?,?)");

            ps.setString(2, name);

            ps.setInt(1, id);
            // ps.setBinaryStream(2, fileContent);
            if (file != null) {
                ps.setBlob(3, file);
                int row = ps.executeUpdate();
                if (row > 0) {
                    return true;
                }
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
        // sends the statement to the database server
    }
    public String getBu_ID (int id){
        String name="";
         try
        {
            PreparedStatement pstmt = con.prepareStatement("Select FBu_ID from image where PIm_ID='"+ id +"'");
            
            ResultSet rs = pstmt.executeQuery();
             
         while (rs.next()) {
                
               
               name = rs.getString("FBu_ID");
               
            }
         return name;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
           
         return name;  
    }
    public String getDescription(int id){
      String name="";
         try
        {
            PreparedStatement pstmt = con.prepareStatement("Select description from image where PIm_ID='"+ id +"'");
            
            ResultSet rs = pstmt.executeQuery();
             
         while (rs.next()) {
                
               
               name = rs.getString("description");
               
            }
         return name;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
           
         return name;    
    }

//    public String deleteImage(String name) {
//        PreparedStatement statement = null;
//        String message = "mahnazzzzz";
//        try {
//            
//            statement = con.prepareStatement("delete from image WHERE description = ?");
//            statement.setString(3, name);
//            // ResultSet rs = statement.execute("select * from image");
//            statement.executeUpdate();
//            int i = statement.executeUpdate();
//            if (i != 0) {
//                message = "Deleting row...";
//            } else if (i == 0) {
//                message = "<br>Row has been deleted successfully";
//            }
//            return message;
//            
//        } catch (SQLException ex) {
//            System.out.println(ex.toString());
//            ex.getStackTrace();
//        }   
//        return message;
//        
//    }
    public String deleteImage (int id){
        String message ="";
            try
        {
            Statement stmt = con.createStatement();
            String delete = "delete from image where PIm_ID='"+ id +"'";
           
            int res =stmt.executeUpdate(delete);
           
            if (res == 0) { message =" image blev ikke slettet";
                return message;}
            else {  message ="image is deletet"; return message;}
        } 
        catch (SQLException ex) 
        {
            System.out.println(ex +" :Error: SQLException i DBFacad deletebuilding");
            
        }
            return message;
        
    }
    public void AllImageForThisBuilding (int buildingID){
     String message ="";
     
            try
        {    
           Statement stmt = con.createStatement();
           String delete = "delete from image where FBu_ID='"+buildingID+"'";
           int res = stmt.executeUpdate(delete);
         
        }catch (SQLException ex) 
        {
            System.out.println(ex +" :Error: SQLException i DBFacad deletebuilding");
            
        }
               
    }
    public byte[] showImage(int index) {

        PreparedStatement statement;
        byte[] content = null;
        try {
            statement = con.prepareStatement("SELECT * FROM image WHERE PIm_ID = ?");
            statement.setInt(1, index);
            // ResultSet rs = statement.execute("select * from image");
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    content = resultSet.getBytes("photo");
                    return content;
                }

                statement.setInt(1, index);
            }
        } catch (SQLException ex) {
            ex.getStackTrace();
        }
        return content;
    }

    public byte[] showImagesBybuildingID(int buildID) {

        PreparedStatement statement;
        byte[] content = null;
        try {
            statement = con.prepareStatement("SELECT * FROM image WHERE FBu_ID = ?");
            statement.setInt(1, buildID);
            // ResultSet rs = statement.execute("select * from image");
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    content = resultSet.getBytes("photo");
                }
                return content;

            }
        } catch (SQLException ex) {
            ex.getStackTrace();
        }
        return content;
    }

    /**
     *
     * @param id
     * @return
     */
    public InputStream showImageByIde(int id) {
        PreparedStatement statement;
        try {
            statement = con.prepareStatement("SELECT * FROM image WHERE PIm_ID ='" + id + "'");
            statement.setInt(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {

                    InputStream photo = resultSet.getBinaryStream("photo");

                }
                return photo;

            }
        } catch (SQLException ex) {
            ex.getStackTrace();
        }
        return photo;

    }

    public List<Integer> ImagesIDInBUILDING(int buildinID) {

        PreparedStatement statement;

        byte[] content = null;
        List<Integer> ides = new ArrayList<Integer>();
        try {
            statement = con.prepareStatement("SELECT * FROM image WHERE FBu_ID = ?");
            statement.setInt(1, buildinID);
            // ResultSet rs = statement.execute("select * from image");
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {

                    int id = resultSet.getInt("PIm_ID");
                    ides.add(id);

                }
                return ides;

            }
        } catch (SQLException ex) {
            ex.getStackTrace();
            System.out.println("here is errorrr " + ex.toString());
        }
        return ides;
    }

}
