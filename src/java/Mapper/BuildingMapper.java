package Mapper;

import DB.DBC;
import Entity.Building;
import Util.Errorhandler;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author LoD
 */
public class BuildingMapper 
{
    public static ArrayList<Building> DisplayAllRows()
    {
        String sql = "Select * from buildings"; 
        ArrayList<Building> blist = new ArrayList<Building>();
        try ( Connection con = DBC.getCon(); 
              Statement stmt = con.createStatement(); 
              ResultSet rs = stmt.executeQuery(sql);) 
        {
            while (rs.next()) 
            {
                Building bean = new Building();
                bean.setBuID(rs.getInt("PBu_ID"));
                bean.setAdresse(rs.getString("address"));
                bean.setZipcode(rs.getString("zip_code"));
                bean.setCity(rs.getString("city"));
                bean.setHousenumber(rs.getString("b_number"));
                bean.setFloor(rs.getString("floor"));
                bean.setSize(rs.getString("size"));
                bean.setRoomamount(rs.getString("number_room"));
                bean.setRoomnumber(rs.getString("room_number"));
                blist.add(bean);
            }
        }
        catch (SQLException ex){            Errorhandler.proExcSQL(ex);        }
        return blist;
    }
    public static Building getRow(int BuildID) 
    {
        String sql = "select * from buildings where PBu_ID = ?"; 
        ResultSet rs = null; 
        try ( Connection con = DBC.getCon(); 
              PreparedStatement stmt =  con.prepareCall(sql);) 
        {
            stmt.setInt(1, BuildID); 
            rs = stmt.executeQuery(); 
            if (rs.next()) 
            { 
                Building bean = new Building(); 
                bean.setBuID(BuildID);
                bean.setAdresse(rs.getString("address"));
                bean.setZipcode(rs.getString("zip_code"));
                bean.setCity(rs.getString("city"));
                bean.setHousenumber(rs.getString("b_number"));
                bean.setFloor(rs.getString("floor"));
                bean.setSize(rs.getString("size"));
                bean.setRoomamount(rs.getString("number_room"));
                bean.setRoomnumber(rs.getString("room_number"));
                return bean; 
            }
            else 
            {
                System.out.println("error");
                return null;
            }
        } 
        catch (SQLException ex) 
        {
            Errorhandler.proExcSQL(ex); 
            return null;
        }
        finally {if (rs != null) try {rs.close();} catch (SQLException ex) { Util.Errorhandler.ExceptionRS(ex); } }
    } 
    public static ArrayList<Building> getRows(int UserID)  
    {
        String sql = "select * from buildings where FUs_ID = ?"; 
        ResultSet rs = null; 
        ArrayList<Building> buildLS = new ArrayList<Building>();
        try ( Connection con = DBC.getCon(); 
              PreparedStatement pstmt = con.prepareStatement(sql);) 
        {
            pstmt.setInt(1, UserID);
            rs = pstmt.executeQuery();
            while (rs.next()) 
            {
                Building bean = new Building();
                bean.setBuID(rs.getInt("PBu_ID"));
                bean.setAdresse(rs.getString("address"));
                bean.setZipcode(rs.getString("zip_code"));
                bean.setCity(rs.getString("city"));
                bean.setHousenumber(rs.getString("b_number"));
                bean.setFloor(rs.getString("floor"));
                bean.setSize(rs.getString("size"));
                bean.setRoomamount(rs.getString("number_room"));
                bean.setRoomnumber(rs.getString("room_number"));
                buildLS.add(bean);
            }
        }
        catch (SQLException ex){            Errorhandler.proExcSQL(ex);        }
        finally {if (rs != null) try {rs.close();} catch (SQLException ex) { Util.Errorhandler.ExceptionRS(ex); } }
        return buildLS;
    } 
    public static boolean insertBuild(Building bean) 
    {
        String sql = "insert into buildings(FUs_ID, address, zip_code, city, b_number, floor, size, number_room, room_number) values (?,?,?,?,?,?,?,?,?)";
        ResultSet keys = null; 
        try (Connection con = DBC.getCon(); 
             PreparedStatement pstmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
        {
            pstmt.setInt(1, bean.getUsID()); 
            pstmt.setString(2, bean.getAdresse()); 
            pstmt.setString(3, bean.getZipcode()); 
            pstmt.setString(4, bean.getCity()); 
            pstmt.setString(5, bean.getHousenumber()); 
            pstmt.setString(6, bean.getFloor()); 
            pstmt.setString(7, bean.getSize()); 
            pstmt.setString(8, bean.getRoomamount()); 
            pstmt.setString(9, bean.getRoomnumber()); 
            int inse = pstmt.executeUpdate(); 
            if (inse == 1) 
            {
                keys = pstmt.getGeneratedKeys(); 
                if (keys.next()) 
                {   
                    int newKey = keys.getInt(1); 
                    bean.setBuID(newKey);
                }
            }
            else
            {
                System.out.println("ingen bruger add til database");
                return false;
            }
        }
        catch (SQLException ex) { Errorhandler.proExcSQL(ex);   return false;   }
        finally {if (keys != null) try {keys.close();} catch (SQLException ex) { Util.Errorhandler.ExceptionRS(ex); } }
        return true;
    } 
    public static boolean updateBuild(Building bean) 
    {
        String sql = "update buildings set address = ?, zip_code = ?, city = ?, b_number = ?, floor = ?, size = ?, number_room = ?, room_number = ? where PBu_ID = ?";
        ResultSet keys = null; 
        try (Connection con = DBC.getCon(); 
             PreparedStatement pstmt = con.prepareStatement(sql);)
        {
            pstmt.setInt(9, bean.getBuID()); 
            pstmt.setString(1, bean.getAdresse()); 
            pstmt.setString(2, bean.getZipcode()); 
            pstmt.setString(3, bean.getCity()); 
            pstmt.setString(4, bean.getHousenumber()); 
            pstmt.setString(5, bean.getFloor()); 
            pstmt.setString(6, bean.getSize()); 
            pstmt.setString(7, bean.getRoomamount()); 
            pstmt.setString(8, bean.getRoomnumber()); 
            int updated = pstmt.executeUpdate();  
            if (updated == 1)   { return true;  } 
            else                { return false; } 
        }
        catch (SQLException ex) {   Errorhandler.proExcSQL(ex);   return false;}
    } 
    public static boolean deleteBuild(int BuildID) 
    {
        String sql = "delete from buildings where PBu_ID = ?";
        try (Connection con = DBC.getCon();
             PreparedStatement pstmt = con.prepareStatement(sql);)
        {
            pstmt.setInt(1, BuildID);
            int delete = pstmt.executeUpdate(); 
            if (delete == 1)   { return true;  }
            else                { return false; }
        }
        catch (SQLException ex) {   Errorhandler.proExcSQL(ex);   return false;}
    }
}
