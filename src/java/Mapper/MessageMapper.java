package Mapper;

import DB.DBC;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MessageMapper {

    Connection con = DBC.getCon();

    public int sendMessage(String name, String message, Date c) {
        PreparedStatement pst;
        int i = 0;
        try {
            pst = con.prepareStatement("insert into costumerMessage(name,message,dato) values(?,?,?)");
            pst.setString(1, name);
            pst.setString(2, message);
            pst.setDate(3, c);

            i = pst.executeUpdate();
        } catch (SQLException ex) {
            ex.getStackTrace();
        }
        return i;
    }

    public String getMessage(String name) {
        PreparedStatement pst;
        String messages = null;

        try {
            pst = con.prepareStatement("SELECT message FROM costumerMessage WHERE name = '" + name + "'");
            pst.setString(1, name);
            // ResultSet rs = statement.execute("select * from image");
            try (ResultSet resultSet = pst.executeQuery()) {
                if (resultSet.next()) {
                    messages = resultSet.getString("message");
                    return messages;
                }

            } catch (SQLException ex) {
                ex.getStackTrace();
            }
        } catch (SQLException ex) {
            ex.getStackTrace();
        }
        return messages;
    }
}