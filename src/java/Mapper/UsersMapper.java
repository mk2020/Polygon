package Mapper;

import DB.DBC;
import Entity.Users;
import Util.Errorhandler;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LoD
 */
public class UsersMapper 
{
    public static void DisplayAllRows() // viser alle rows som Strings
    {
        String sql = "Select PUs_ID, Userstype, Username from users"; 
        try ( Connection con = DBC.getCon(); 
              Statement stmt = con.createStatement(); 
              ResultSet rs = stmt.executeQuery(sql);) 
        {
            System.out.println("Bruger Table");
            while (rs.next()) 
            {
                StringBuilder sb = new StringBuilder();  
                sb.append("Bruger ID   : "+rs.getInt("PUs_ID") + " :");
                sb.append("Bruger Name : "+rs.getString("Username")+ " :");
                sb.append("Bruger Type : "+rs.getString("Userstype")+ " :"); 
                System.out.println(sb.toString()); 
            }
        }
        catch (SQLException ex){            Errorhandler.proExcSQL(ex);        } 
    } 
    public static Users getRow(int UserID)
    {
        String sql = "select * from users where PUs_ID = ?"; 
        ResultSet rs = null; 
        try ( Connection con = DBC.getCon(); 
              PreparedStatement stmt =  con.prepareCall(sql);) 
        {
            stmt.setInt(1, UserID); 
            rs = stmt.executeQuery(); 
            if (rs.next()) 
            { 
                Users bean = new Users(); 
                bean.setUserID(UserID); 
                bean.setUserName(rs.getString("Username")); 
                bean.setUserType(rs.getInt("Userstype"));
                return bean; 
            }
            else 
            {
                return null;
            }
        } 
        catch (SQLException ex) 
        {
            Errorhandler.proExcSQL(ex); 
            return null;
        }
        finally {if (rs != null) try {rs.close();} catch (SQLException ex) { Util.Errorhandler.ExceptionRS(ex); } }
    }
    public static ArrayList<Users> getRows(int UserType)
    {
        String sql = "Select PUs_ID, Userstype, Username from users where Userstype= ?";
        ResultSet rs = null;
        ArrayList<Users> usersLS = new ArrayList<Users>();
        try ( Connection con = DBC.getCon(); 
              PreparedStatement stmt =  con.prepareCall(sql);) 
        {
            stmt.setInt(1, UserType); 
            rs = stmt.executeQuery(); 
            while (rs.next()) 
            {
                Users bean = new Users();
                bean.setUserID(rs.getInt("PUs_ID"));
                bean.setUserType(rs.getInt("Userstype"));
                bean.setUserName(rs.getString("Username"));
                usersLS.add(bean);
            }
        }
        catch (SQLException ex){            Errorhandler.proExcSQL(ex);        }
        finally {if (rs != null) try {rs.close();} catch (SQLException ex) { Util.Errorhandler.ExceptionRS(ex); } }
        return usersLS;
    } 
    public static boolean insertUser(Users bean)
    {
        String sql = "insert into users (Userstype, Username, Upassword) values (?,?,?)"; 
        ResultSet keys = null; 
        try (Connection con = DBC.getCon(); 
             PreparedStatement pstmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);)
        {
            pstmt.setInt(1, bean.getUserType()); 
            pstmt.setString(2, bean.getUserName()); 
            pstmt.setString(3, bean.getPassword()); 
            int inse = pstmt.executeUpdate(); 
            if (inse == 1) 
            {
                keys = pstmt.getGeneratedKeys();
                if (keys.next()) 
                {   
                    int newKey = keys.getInt(1);
                    bean.setUserID(newKey); 
                }
            }
            else
            {
                System.out.println("ingen bruger add til database");
                return false;
            }
        }
        catch (SQLException ex) { Errorhandler.proExcSQL(ex);   return false;   }
        finally {if (keys != null) try {keys.close();} catch (SQLException ex) { Util.Errorhandler.ExceptionRS(ex); } }
        return true;
    } 
    public static boolean updateUser(Users bean) 
    {
        String sql = "update users set Userstype = ? where PUs_ID = ?"; 
        try (Connection con = DBC.getCon();
             PreparedStatement pstmt = con.prepareStatement(sql);)
        {
            pstmt.setInt(1, bean.getUserType());
            pstmt.setInt(2, bean.getUserID());
            
            int updated = pstmt.executeUpdate();  
            if (updated == 1)   { return true;  } 
            else                { return false; } 
        }
        catch (SQLException ex) {   Errorhandler.proExcSQL(ex);   return false;}
    } 
    public static boolean deleteUser(int UserID) 
    {
        String sql = "delete from users where PUs_ID = ?";
        try (Connection con = DBC.getCon();
             PreparedStatement pstmt = con.prepareStatement(sql);)
        {
            pstmt.setInt(1, UserID);
            int delete = pstmt.executeUpdate(); 
            if (delete == 1)   { return true;  }
            else                { return false; }
        }
        catch (SQLException ex) {   Errorhandler.proExcSQL(ex);   return false;}
    }
    public static int[] validate (String username, String password)
    {
        int[] UI = new int[2];
        try (Connection con = DBC.getCon();) 
        {  
            String valiSQL = "select * from users where Username= ? and Upassword= ?";
            PreparedStatement pstmt = con.prepareCall(valiSQL);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next())
            {
                UI[0] = rs.getInt("Userstype");
                UI[1] = rs.getInt("PUs_ID");
            }
            return UI;
        }
        catch(SQLException ex) 
        {
            System.out.println(ex + " :Error: SQLException fejl i DBFacad validate");
            UI[0] = 0;
            UI[1] = 0;
            return UI;
        }
    }
}    
