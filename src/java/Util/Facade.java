package Util;

import DB.DBC;
import Entity.Building;
import Entity.Image;
import Entity.Users;
import Mapper.BuildingMapper;
import Mapper.ImageMapper;
import Mapper.UsersMapper;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author LoD
 */
public class Facade 
{
    Connection con = DBC.getCon();
    ImageMapper im = new ImageMapper();
    public static int[] validate (String username, String password)     {return UsersMapper.validate(username, password);}
    public static ArrayList<Users> getUserRows(int UserType)            {return UsersMapper.getRows(UserType);           }
    public static boolean insertUser(Users bean)                        {return UsersMapper.insertUser(bean);            }
    public static boolean updateUser(Users bean)                        {return UsersMapper.updateUser(bean);            }
    public static boolean deleteUser(int UserID)                        {return UsersMapper.deleteUser(UserID);          }
    
    public static ArrayList<Building> listofbuilding()          {return BuildingMapper.DisplayAllRows(); }
    public static Building getBuilding(int BuildID)                  {return BuildingMapper.getRow(BuildID);  }
    public static ArrayList<Building> getBuildRows(int UserID)  {return BuildingMapper.getRows(UserID);  }
    public static boolean insertBuilding (Building bean)        {return BuildingMapper.insertBuild(bean);}
    public static boolean updateBuilding(Building bean)         {return BuildingMapper.updateBuild(bean);}
    public static boolean deletebuildingID (int b_ID)           {return BuildingMapper.deleteBuild(b_ID);}
    
    public ArrayList<Image> getAllImagesDescription() {
         return im.getAllImagesDescription();
     } 
    public boolean saveImage(int id, String name, InputStream file) {
        return im.saveImage(id, name, file);
    }
     public String getBu_ID (int id){
        return im.getBu_ID(id);
     }
      public String getDescription(int id){
          return im.getDescription(id);
      }
      public String deleteImage (int id){
          return im.deleteImage(id);
      }
    public void AllImageForThisBuilding (int buildingID){  
        
    }
    public byte[] showImage(int index) {
        return im.showImage(index);
    }
    public byte[] showImagesBybuildingID(int buildID) {
        return im.showImagesBybuildingID(buildID);
    }
     public List<Integer> ImagesIDInBUILDING(int buildinID) {
         return im.ImagesIDInBUILDING(buildinID);
     }
    public boolean deletebuilding (String b_address)
    {
        try
        {
            Statement stmt = con.createStatement();
            String deletebuild = "delete from buildings where address='"+b_address+"'";
            int res = stmt.executeUpdate(deletebuild);
            if (res == 1) {return true;}
            else {return false;}
        } 
        catch (SQLException ex) 
        {
            System.out.println(ex +" :Error: SQLException i DBFacad deletebuilding");
            return false;
        }
    }
}