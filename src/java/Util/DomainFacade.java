package Util;

import Entity.Building;
import Entity.Image;
import Entity.Users;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LoD
 */
public class DomainFacade 
{ Facade f = new Facade();
    public int[] validate (String username, String password)    {return Facade.validate(username, password);    }    
    public ArrayList<Users> getUserType(int UserType)           {return Facade.getUserRows(UserType);           }
    public boolean addUser(Users bean)                          {return Facade.insertUser(bean);                }
    public boolean editUser(Users bean)                         {return Facade.updateUser(bean);                }
    public boolean deleteUser(int UserID)                       {return Facade.deleteUser(UserID);              }
    
    public ArrayList<Building> listofbuilding()                 {return Facade.listofbuilding();                }    
    public ArrayList<Building> getUsersBuilding(int UserID)     {return Facade.getBuildRows(UserID);            }
    public Building Build(int BuildID)                          {return Facade.getBuilding(BuildID);            }
    public boolean addBuilding (Building bean)                  {return Facade.insertBuilding(bean);            }
    public boolean editBuilding(Building bean)                  {return Facade.updateBuilding(bean);            }
    public boolean deleteBuilding (int b_ID)                    {return Facade.deletebuildingID(b_ID);          }
     public ArrayList<Image> getAllImagesDescription() {
         return f.getAllImagesDescription();
     } 
    public boolean saveImage(int id, String name, InputStream file) {
        return f.saveImage(id, name, file);
    }
     public String getBu_ID (int id){
        return f.getBu_ID(id);
     }
      public String getDescription(int id){
          return f.getDescription(id);
      }
      public String deleteImage (int id){
          return f.deleteImage(id);
      }
    public void AllImageForThisBuilding (int buildingID){  
        
    }
    public byte[] showImage(int index) {
        return f.showImage(index);
    }
    public byte[] showImagesBybuildingID(int buildID) {
        return f.showImagesBybuildingID(buildID);
    }
     public List<Integer> ImagesIDInBUILDING(int buildinID) {
         return f.ImagesIDInBUILDING(buildinID);
     }
}
