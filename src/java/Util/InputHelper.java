package Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LoD
 */
public class InputHelper 
{
    public static String getInput(String promt)
    {
        BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(promt);
        System.out.flush();
        try 
        {
            return bfr.readLine();
        } 
        catch (IOException ex) 
        {
            return "error" + ex.getMessage();
        }
    }
    public static int getIntegerInput(String promt)
    {
        String input = getInput(promt);
        return Integer.parseInt(input);
    }
}
