package Entity;

import java.io.InputStream;

public class Image {
    private int id;
    private int B_id;
    private String name;
    private InputStream photo;

    
    public Image(int id, String name, InputStream photo) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        }

    public Image(String name, InputStream photo) {
        this.name = name;
        this.photo = photo;
    }
    public int getB_id()                    {        return B_id;           }
    public void setB_id(int B_id)           {        this.B_id = B_id;      }
    public int getId()                      {        return id;             }
    public String getName()                 {        return name;           }
    public void setName(String name)        {        this.name = name;      }
    public InputStream getPhoto()           {        return photo;          }
    public void setPhoto(InputStream photo) {        this.photo = photo;    }
}
