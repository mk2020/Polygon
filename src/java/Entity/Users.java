package Entity;
/**
 *
 * @author LoD
 */
public class Users 
{
    int userID, userType;
    String userName, password;
    
    @Override
    public String toString()
    {
        return "brugers id : "+userID+" bruger type : "+userType+" brugers navn : "+userName;
    }
    
    public int getUserID()                      {        return userID;             }
    public int getUserType()                    {        return userType;           }
    public String getUserName()                 {        return userName;           }
    public String getPassword()                 {        return password;           }
    public void setUserID(int userID)           {        this.userID = userID;      }
    public void setUserType(int userType)       {        this.userType = userType;  }
    public void setUserName(String userName)    {        this.userName = userName;  }
    public void setPassword(String password)    {        this.password = password;  }
}
