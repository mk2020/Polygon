package Entity;
import java.util.ArrayList;
/**
 *
 * @author LoD
 */
public class Building 
{
    private String adresse, zipcode, city, housenumber , floor, size, roomamount, roomnumber;
    private int buID, usID;
    private ArrayList<Image> Img;

    @Override
    public String toString()
    {
        return ": "+adresse+" : "+zipcode+" : "+city +" : "+housenumber+" : "+floor+" : "+size+" : "+roomamount+" : "+roomnumber+" : \n";
    }
    public String toString2()
    {
        return buID+" "+adresse+" "+zipcode+" "+city +" "+housenumber+" "+floor+" "+size+" "+roomamount+" "+roomnumber+"\n";
    }
    public String getAdresse()      {return adresse;        }
    public String getZipcode()      {return zipcode;        }
    public String getCity()         {return city;           }
    public String getHousenumber()  {return housenumber;    }
    public String getFloor()        {return floor;          }
    public String getSize()         {return size;           }
    public String getRoomamount()   {return roomamount;     }
    public String getRoomnumber()   {return roomnumber;     }
    public int getBuID()            {return buID;           }
    public int getUsID()            {return usID;           }

    public void setAdresse(String adresse)          {this.adresse = adresse;        }
    public void setZipcode(String zipcode)          {this.zipcode = zipcode;        }
    public void setCity(String city)                {this.city = city;              }
    public void setHousenumber(String housenumber)  {this.housenumber = housenumber;}
    public void setFloor(String floor)              {this.floor = floor;            }
    public void setSize(String size)                {this.size = size;              }
    public void setRoomamount(String roomamount)    {this.roomamount = roomamount;  }
    public void setRoomnumber(String roomnumber)    {this.roomnumber = roomnumber;  }
    public void setBuID(int buID)                   {this.buID = buID;              }
    public void setUsID(int usID)                   {this.usID = usID;              }
}