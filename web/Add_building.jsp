<%-- 
    Document   : Add_building
    Created on : Oct 31, 2016, 2:37:05 PM
    Author     : LoD
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Tilføje Bygning</h1>
        <form method="POST" action="Byggninghandle">
            <table border="0">
                <thead>
                    <tr>
                        <td>Adresse</td>
                        <th><input type="text" name="address" value="" required=""/></th>
                    </tr>
                    <tr>
                        <td>Post nummer</td>
                        <td><input type="text" name="zip_code" value="" required=""/></td>
                    </tr>
                    <tr>
                        <td>By</td>
                        <td><input type="text" name="city" value="" required=""  /></td>
                    </tr>
                    <tr>
                        <td>Hus Nummer</td>
                        <td><input type="text" name="b_number" value="" required="" /></td>
                    </tr>
                    <tr>
                        <td>Etage(r)</td>
                        <td><input type="text" name="floor" /></td>
                    </tr>
                    <tr>
                        <td>M^2</td>
                        <td><input type="text" name="size" /></td>
                    </tr>
                    <tr>
                        <td>Antal rum</td>
                        <td><input type="text" name="number_room" /></td>
                    </tr>
                    <tr>
                        <td>Rum nummer</td>
                        <td><input type="text" name="room_number" value="" /></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Tilføje" name="add-B-unit" /></td>
                        <td><input type="reset" value="Nulstil" name="add-B-unit" /></td>
                    </tr>
                </thead>
            </table>
            <input type="hidden" name="type" value="add" />
        </form>
        <form action="Loginhandle" method="POST">
             <input type="hidden" name="origin" value="backtostart" />
             <input type="submit" value="Front siden" name="submit" />
        </form>
    </body>
</html>
