<%-- 
    Document   : Byninglist
    Created on : Oct 31, 2016, 1:26:00 PM
    Author     : LoD
--%>


<%@page import="java.util.ArrayList"%>
<%@page import="Entity.Building"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>mom's spaghetti</title>
        <style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}
</style>
    </head>
    <body>
       
        <table >
           
                <tr>
                    <th>Adresse</th>
                    <th>Post Nr.</th>
                    <th>By</th>
                    <th>Hus Nr.</th>
                    <th>Etage(r)</th>
                    <th>Rum Meter</th>
                    <th>Antal Rum</th>
                    <th>Rum Nr.</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Show Image</th>
                    <th>opload Image</th>
                </tr>
                
                <%
                    ArrayList<Building> al = (ArrayList) session.getAttribute("buildingList");
                    for (int i = 0; i < al.size(); i++)
                    {%>
                    <tr>
                        <%
                        Building oneArrLis = al.get(i);
                        String adre = oneArrLis.getAdresse();
                        String cit = oneArrLis.getCity();
                        String flo = oneArrLis.getFloor();
                        String hou = oneArrLis.getHousenumber();
                        String roa = oneArrLis.getRoomamount();
                        String ron = oneArrLis.getRoomnumber();
                        String siz = oneArrLis.getSize();
                        String zip = oneArrLis.getZipcode();
                        int BID = oneArrLis.getBuID();
                        out.print("<td>"+adre+"</td>");
                        out.print("<td>"+zip+"</td>");
                        out.print("<td>"+cit+"</td>");
                        out.print("<td>"+hou+"</td>");
                        out.print("<td>"+flo+"</td>");
                        out.print("<td>"+siz+"</td>");
                        out.print("<td>"+roa+"</td>");
                        out.print("<td>"+ron+"</td>");
                        out.print("<td><a href="+"Byggninghandle?type=getbuilding&SELBUI="+BID+""+"><button>Edit</button></a></td>");
                        
                        //out.print("<td><a href="+"Byggninghandle?type=delete&SELBUI="+BID+""+"><button>DELETE</button></a></td>");
                        out.print("<td><a href="+"Byggninghandle?type=delete&SELBUI="+BID+""+"><button>DELETE</button></a></td>");
                        //session.setAttribute("builidingPic", adre);
                        out.print("<td><a href="+"result.jsp?buildinpPic="+BID+""+"><button>Show Image</button></a></td>");
                        out.print("<td><a href=upload.jsp?id="+BID+"><button>opload</button></a></td>");
                        out.print("<td></td>");%>
                     </tr>
                        <%}
                %>
                
           
           
        </table>
        <form action="Loginhandle" method="POST">
             <input type="hidden" name="origin" value="backtostart" />
             <input type="submit" value="Front siden" name="submit" />
        </form>
    </body>
</html>