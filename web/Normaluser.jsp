<%
    if (session.getAttribute("usertype") == null)    {response.sendRedirect("index.html");    }
    //else if ((int)session.getAttribute("usertype") == 2) {response.sendRedirect("polyglogin.jsp");}
    //else if ((int)session.getAttribute("usertype") == 3) {response.sendRedirect("adminlogin.jsp");}
%>
<%-- 
    Document   : Normaluser
    Created on : Oct 27, 2016, 11:09:38 AM
    Author     : LoD
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Velkomme <%= request.getSession(false).getAttribute("Username")%></title>
    </head>
    <body>
        <h1>Velkomme <%= request.getSession(false).getAttribute("Username")%> og type bruger du er : <%= request.getSession(false).getAttribute("usertype")%></h1>
        <form action="Byggninghandle" method="POST">
             <input type="hidden" name="type" value="listBuildings" />
             <input type="submit" value="Byggning" class="btn btn-primary" name="submit" />
        </form>
        <a href="Add_building.jsp"><button type="button" class="btn btn-primary active">Tilføje Bygning</button></a>
        <form action="Loginhandle" method="POST">
             <input type="hidden" name="origin" value="logout" />
             <input type="submit" value="log ude" class="btn btn-primary disabled" name="submit" />
        </form>
    </body>
</html>