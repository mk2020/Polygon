<%
    if (session.getAttribute("usertype") == null)    {response.sendRedirect("PolygonFrontPage.html");    }
    else if ((int)session.getAttribute("usertype") == 1) {response.sendRedirect("AdminFrontPage.jsp");}
    else if ((int)session.getAttribute("usertype") == 3) {response.sendRedirect("TechnicianFrontPage.jsp");}
%>
<%-- 
    Document   : PolygonMedarbejderLogin
    Created on : 02-11-2016, 12:57:56
    Author     : Anders
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Polygon medarbejder Login</h1>
        <form action="Loginhandle" method="POST">
            <div class="imgcontainer">
            <img id="polygonImage" src="http://www.polygongroup.com/globalassets/nederlands-nederland/media/fotos/logos-e.d/polygon-logo-hi-res.jpg" style="width: 1200px; height: 400px;">
            </div>
        <label><b>Bruger</b></label>
        <input type="text" placeholder="Enter Username" name="username" required>
        <label><b>Adgangskode</b></label>
            <input type="password" placeholder="Enter Password" name="password" required>
            <input type="hidden" name="origin" value="login" />
            <input type="submit" value="Login" name="submit" />
        </form>
    </body>
</html>
