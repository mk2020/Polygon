<%-- 
    Document   : Edit_Building
    Created on : Dec 1, 2016, 10:23:15 AM
    Author     : LoD
--%>
<%@page import="Entity.Building"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% Building Bying2 = (Building) session.getAttribute("SelectBuilding"); %>
        <form method="POST" action="Byggninghandle">
            <table border="0">
                <thead>
                    <tr>
                        <td>Adresse</td>
                        <th><input type="text" name="address" value="<%= Bying2.getAdresse() %>" placeholder="<%= Bying2.getAdresse() %>" /></th>
                    </tr>
                    <tr>
                        <td>Post nummer</td>
                        <td><input type="text" name="zip_code" value="<%= Bying2.getZipcode() %>" placeholder="<%= Bying2.getZipcode() %>"/></td>
                    </tr>
                    <tr>
                        <td>By</td>
                        <td><input type="text" name="city" value="<%= Bying2.getCity() %>" placeholder="<%= Bying2.getCity() %>"/></td>
                    </tr>
                    <tr>
                        <td>Hus Nummer</td>
                        <td><input type="text" name="b_number" value="<%= Bying2.getHousenumber() %>" placeholder="<%= Bying2.getHousenumber() %>"/></td>
                    </tr>
                    <tr>
                        <td>Etage(r)</td>
                        <td><input type="text" name="floor" value="<%= Bying2.getFloor() %>" placeholder="<%= Bying2.getFloor() %>"/></td>
                    </tr>
                    <tr>
                        <td>M^2</td>
                        <td><input type="text" name="size" value="<%= Bying2.getSize() %>" placeholder="<%= Bying2.getSize() %>"/></td>
                    </tr>
                    <tr>
                        <td>Antal rum</td>
                        <td><input type="text" name="number_room" value="<%= Bying2.getRoomamount() %>" placeholder="<%= Bying2.getRoomamount() %>"/></td>
                    </tr>
                    <tr>
                        <td>Rum nummer</td>
                        <td><input type="text" name="room_number" value="<%= Bying2.getRoomnumber() %>" placeholder="<%= Bying2.getRoomnumber() %>"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Tilføje" name="add-B-unit" /></td>
                        <td><input type="reset" value="Nulstil" name="add-B-unit" /></td>
                    </tr>
                </thead>
            </table>
            <input type="hidden" name="BuildingID" value="<%= Bying2.getBuID() %>" />
            <input type="hidden" name="type" value="editBuilding" />
        </form>
            <form action="Loginhandle" method="POST">
             <input type="hidden" name="origin" value="AdminBackToStart" />
             <input type="submit" value="Forside" name="submit" />
        </form>
    </body>
</html>
