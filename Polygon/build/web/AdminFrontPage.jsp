<%
    if (session.getAttribute("usertype") == null)    {response.sendRedirect("PolygonFrontPage.html");}
    else if ((int)session.getAttribute("usertype") == 2) {response.sendRedirect("CustomerFrontPage.jsp");}
    else if ((int)session.getAttribute("usertype") == 3) {response.sendRedirect("TechnicianFrontPage.jsp");}
%>

<%
session.setMaxInactiveInterval(2);
%>

<%-- 
    Document   : AdminUser
    Created on : Oct 27, 2016, 11:09:38 AM
    Author     : LoD
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Velkommen <%= request.getSession(false).getAttribute("Username")%></title>
        <script type="text/javascript">
            var Msg ='<%=session.getAttribute("getMailSendt")%>';
            if (Msg !== "null") {
                function alertName(){
                    alert("Din mail er blevet sendt");
                } 
            }
        </script>
    </head>
    <body>
        <h1>Velkommen <%= request.getSession(false).getAttribute("Username")%>. Du er brugertype : <%= request.getSession(false).getAttribute("usertype")%></h1>
        <script type="text/javascript"> window.onload = alertName; </script>   
        <div class="background">
            <div style="position: absolute; left: 80px; top: 150px;">
                <img src="http://www.polygongroup.com/globalassets/nederlands-nederland/media/fotos/logos-e.d/polygon-logo-hi-res.jpg" style="width: 1200px; height: 400px;">
            </div>
        </div>
        <div class="buttons">
            <div style="position: absolute; top: 1px; left: 1px;">
                <form action="BuildingHandler" method="POST">
                     <input type="hidden" name="type" value="listBuildings" />
                     <input type="submit" value="Alle bygninger" name="submit" />
                </form>
            </div>
            <div style="position: absolute; top: 1px; left: 110px;">
                <a href="AddBuilding.jsp"><button type="button">Tilføj Bygning</button></a>
            </div>
            <div style="position: absolute; top: 1px; left: 216px;">
                <form action="BuildingHandler" method="POST">
                     <input type="hidden" name="type" value="delete" />
                     <input type="submit" value="Slet bygning" name="submit" />
                </form>
            </div>
            <div style="position: absolute; top: 1px; left: 314px;">
                <a href="EmailSend.jsp"><button type="button">Send email</button></a>
            </div>
            <div style="position: absolute; top: 1px; right: 1px;">
                <form action="Loginhandle" method="POST">
                    <input type="hidden" name="origin" value="logout" />
                    <input type="submit" value="Log af" name="submit" />
                </form>
            </div>
        </div>
    </body>
</html>