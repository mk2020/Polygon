<%
    if (session.getAttribute("usertype") == null)    {response.sendRedirect("PolygonFrontPage.html");    }
    else if ((int)session.getAttribute("usertype") == 2) {response.sendRedirect("CustomerFrontPage.jsp");}
    else if ((int)session.getAttribute("usertype") == 3) {response.sendRedirect("TechnicianFrontPage.jsp");}
%>
<%-- 
    Document   : Add_building
    Created on : Oct 31, 2016, 2:37:05 PM
    Author     : LoD
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tilføj Ny Bygning</title>
    </head>
    <body>
        <h1>Tilføj Bygning</h1>
        <form method="POST" action="BuildingHandler">
            <table border="0">
                <thead>
                    <tr>
                        <td>Adresse</td>
                        <th><input type="text" name="address" value="" /></th>
                    </tr>
                    <tr>
                        <td>Post nummer</td>
                        <td><input type="text" name="zip_code" value=""/></td>
                    </tr>
                    <tr>
                        <td>By</td>
                        <td><input type="text" name="city" value="" /></td>
                    </tr>
                    <tr>
                        <td>Hus Nummer</td>
                        <td><input type="text" name="b_number" value="" /></td>
                    </tr>
                    <tr>
                        <td>Etage(r)</td>
                        <td><input type="text" name="floor" /></td>
                    </tr>
                    <tr>
                        <td>M^2</td>
                        <td><input type="text" name="size" /></td>
                    </tr>
                    <tr>
                        <td>Antal rum</td>
                        <td><input type="text" name="number_room" /></td>
                    </tr>
                    <tr>
                        <td>Rum nummer</td>
                        <td><input type="text" name="room_number" value="" /></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Tilføj" name="add-B-unit" /></td>
                        <td><input type="reset" value="Nulstil" name="add-B-unit" /></td>
                    </tr>
                </thead>
            </table>
            <input type="hidden" name="type" value="add" />
        </form>
        <form action="Loginhandle" method="POST">
             <input type="hidden" name="origin" value="AdminBackToStart" />
             <input type="submit" value="Forside" name="submit" />
        </form>
    </body>
</html>
