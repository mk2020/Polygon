<%-- 
    Document   : PendingFiles
    Created on : Dec 1, 2016, 2:41:41 PM
    Author     : LoD
--%>
<%@page import="Entity.Pending"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Util.DomainFacade"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            DomainFacade DF = new DomainFacade();
        if ( DF.listofstuff() == null ){}
                ArrayList<Pending> LOP = DF.listofstuff();
        if (LOP.size() != 0)
        {
            for (int i = 0 ; i < LOP.size(); ++i)
            {
                Pending exetype = LOP.get(i);
                String exe = exetype.getExe();
                if (exe.equalsIgnoreCase("add"))
                {
                %>
                <h1>Ny Bygning </h1>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Navn</th>
                            <th>Adresse</th>
                            <th>Post Nr.</th>
                            <th>By</th>
                            <th>Hus Nr.</th>
                            <th>Etage(r)</th>
                            <th>Rum Meter</th>
                            <th>Antal Rum</th>
                            <th>Rum Nr.</th>
                            <th>Bruger ID</th>
                            <th>Godkend</th>
                            <th>Afkast</th>
                        </tr>
                        <tr>
                            <th><%= exetype.getUNa() %></th>
                            <th><%= exetype.getAdd() %></th>
                            <th><%= exetype.getZip()%></th>
                            <th><%= exetype.getCity()%></th>
                            <th><%= exetype.getBn()%></th>
                            <th><%= exetype.getFlo()%></th>
                            <th><%= exetype.getSize()%></th>
                            <th><%= exetype.getRa()%></th>
                            <th><%= exetype.getRn()%></th>
                            <th><%= exetype.getUid()%></th>
                            <th><button>OK</button></th>
                            <th><button>Afkast</button></th>
                        </tr>
                    </thead>
                </table>
                <%
                }
                else if (exe.equalsIgnoreCase("edit"))
                {
                %>
                <h1>Edit i Bygning </h1>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Navn</th>
                            <th>Adresse</th>
                            <th>Post Nr.</th>
                            <th>By</th>
                            <th>Hus Nr.</th>
                            <th>Etage(r)</th>
                            <th>Rum Meter</th>
                            <th>Antal Rum</th>
                            <th>Rum Nr.</th>
                            <th>Bruger ID</th>
                            <th>Godkend</th>
                            <th>Afkast</th>
                        </tr>
                        <tr>
                            <th><%= exetype.getUNa() %></th>
                            <th><%= exetype.getAdd() %></th>
                            <th><%= exetype.getZip()%></th>
                            <th><%= exetype.getCity()%></th>
                            <th><%= exetype.getBn()%></th>
                            <th><%= exetype.getFlo()%></th>
                            <th><%= exetype.getSize()%></th>
                            <th><%= exetype.getRa()%></th>
                            <th><%= exetype.getRn()%></th>
                            <th><%= exetype.getUid()%></th>
                            <th><button>OK</button></th>
                            <th><button>Afkast</button></th>
                        </tr>
                    </thead>
                </table>
                <%
                }
                else if (exe.equalsIgnoreCase("delete"))
                {
                %>
                <h1>Slet Bygning </h1>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Navn</th>
                            <th>Adresse</th>
                            <th>Post Nr.</th>
                            <th>By</th>
                            <th>Hus Nr.</th>
                            <th>Etage(r)</th>
                            <th>Rum Meter</th>
                            <th>Antal Rum</th>
                            <th>Rum Nr.</th>
                            <th>Bruger ID</th>
                            <th>Godkend</th>
                            <th>Afkast</th>
                        </tr>
                        <tr>
                            <th><%= exetype.getUNa() %></th>
                            <th><%= exetype.getAdd() %></th>
                            <th><%= exetype.getZip()%></th>
                            <th><%= exetype.getCity()%></th>
                            <th><%= exetype.getBn()%></th>
                            <th><%= exetype.getFlo()%></th>
                            <th><%= exetype.getSize()%></th>
                            <th><%= exetype.getRa()%></th>
                            <th><%= exetype.getRn()%></th>
                            <th><%= exetype.getUid()%></th>
                            <th><button>OK</button></th>
                            <th><button>Afkast</button></th>
                        </tr>
                    </thead>
                </table>
                <%
                }
            }
        }
        else{ out.write("der er ikke nogen pending byning lige nu");}
        %>
    </body>
</html>