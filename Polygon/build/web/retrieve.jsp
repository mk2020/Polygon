
<%-- 
    Document   : retrieve
    Created on : Nov 10, 2016, 1:36:12 PM
    Author     : mahnaz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List, Entity.Image, Mapper.ImageMapper" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% List<Image> images = new ImageMapper().getAllImages(); %>
       
        <h1>show image!</h1>
        <form name="show" action="ImageViewer">
        <select name="id">
        <% for(int i = 0; i < images.size(); i++) {
            %>
            <option  value="<%out.print(images.get(i).getId());%>">
                <% out.print(images.get(i).getName()); %></option>
            <%
        }%>
        </select>
         <input type="submit" value="show picture"/>
        </form>
    </body>
</html>