<%
//    if (session.getAttribute("usertype") == null)    {   response.sendRedirect("index.html");    }
%>
<%
    if (session.getAttribute("usertype") == null)    {response.sendRedirect("PolygonFrontPage.html");    }
    else if ((int)session.getAttribute("usertype") == 2) {response.sendRedirect("CustomerFrontPage.jsp");}
    else if ((int)session.getAttribute("usertype") == 3) {response.sendRedirect("TechnicianFrontPage.jsp");}
%>
<%-- 
    Document   : Delete_building
    Created on : Nov 8, 2016, 11:57:38 AM
    Author     : LoD
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Slet Bygning</title>
    </head>
    <%= request.getSession(false).getAttribute("something") %>
    <body>
        <form method="POST" action="BuildingHandler">
            <!--Byggninghandle-->
            <select name="choseBuilding">
                <% 
                    ArrayList al = (ArrayList) session.getAttribute("buildingList");
                    boolean add = false;
                    for (String retval: al.toString().split(":")) 
                    {
                        if (add == true)
                        {
                            out.print("<option>"+retval+"</option>");
                            add = false;
                        }
                        if (retval.equalsIgnoreCase(", ") || retval.equalsIgnoreCase("["))
                        {
                            add = true;
                        }
                    }
                %>
            </select>
            <input type="hidden" name="type" value="delete" />
            <input type="submit" value="Slet bygning" />
        </form>
        <a href="Normaluser.jsp"><button type="button">Tilbage til start</button></a>
        <h1>Hello World!</h1>
        <form action="Loginhandle" method="POST">
             <input type="hidden" name="origin" value="AdminBackToStart" />
             <input type="submit" value="Forside" name="submit" />
        </form>
    </body>
</html>
