<%
//    if (session.getAttribute("usertype") == null)    {response.sendRedirect("PolygonFrontPage.html");    }
//    else if ((int)session.getAttribute("usertype") == 2) {response.sendRedirect("CustomerFrontPage.jsp");}
//    else if ((int)session.getAttribute("usertype") == 3) {response.sendRedirect("TechnicianFrontPage.jsp");}
%>
<%-- 
    Document   : success
    Created on : Nov 8, 2016, 2:09:26 PM
    Author     : Thomas Hartmann - tha@cphbusiness.dk
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript">
            var Msg ='<%=session.getAttribute("getMailSendt")%>';
            if (Msg !== "null") {
                function alertName(){
                    alert("Din mail er blevet sendt");
                } 
            }
        </script>
    </head>
    <body>
        <h1>Success</h1>
        <h1>Velkommen <%= request.getSession(false).getAttribute("Username")%>. Du er brugertype : <%= request.getSession(false).getAttribute("usertype")%></h1>
        <script type="text/javascript"> window.onload = alertName; </script>
        You have send an email with the following content:
        <% out.println(request.getParameter("body")); %>
    </body>
</html>
