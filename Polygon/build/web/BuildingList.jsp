<%
    if (session.getAttribute("usertype") == null)    {response.sendRedirect("PolygonFrontPage.html");    }
    else if ((int)session.getAttribute("usertype") == 2) {response.sendRedirect("CustomerFrontPage.jsp");}
    else if ((int)session.getAttribute("usertype") == 3) {response.sendRedirect("TechnicianFrontPage.jsp");}
%>

<%-- 
    Document   : Byninglist
    Created on : Oct 31, 2016, 1:26:00 PM
    Author     : LoD
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="Entity.Building"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alle bygninger fra liste</title>
    </head>
    <body>
        <table border="1">
            <thead>
                <tr>
                    <th>Adresse</th>
                    <th>Post Nr.</th>
                    <th>By</th>
                    <th>Hus Nr.</th>
                    <th>Etage(r)</th>
                    <th>Rum Meter</th>
                    <th>Antal Rum</th>
                    <th>Rum Nr.</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <%
                    ArrayList<Building> al = (ArrayList) session.getAttribute("buildingList");
                    for (int i = 0; i < al.size(); i++)
                    {
                        Building oneArrLis = al.get(i);
                        String adre = oneArrLis.getAdresse();
                        String cit = oneArrLis.getCity();
                        String flo = oneArrLis.getFloor();
                        String hou = oneArrLis.getHousenumber();
                        String roa = oneArrLis.getRoomamount();
                        String ron = oneArrLis.getRoomnumber();
                        String siz = oneArrLis.getSize();
                        String zip = oneArrLis.getZipcode();
                        int BID = oneArrLis.getBuID();
                        out.print("<th>"+adre+"</th>");
                        out.print("<th>"+zip+"</th>");
                        out.print("<th>"+cit+"</th>");
                        out.print("<th>"+hou+"</th>");
                        out.print("<th>"+flo+"</th>");
                        out.print("<th>"+siz+"</th>");
                        out.print("<th>"+roa+"</th>");
                        out.print("<th>"+ron+"</th>");
                        out.print("<th><a href="+"BuildingHandler?type=getbuilding&SELBUI="+BID+""+"><button>Edit</button></a></th>");
                        out.print("<th><a href="+"BuildingHandler?type=delete&SELBUI="+BID+""+"><button>DELETE</button></a></th>");
                        out.print("<tr></tr>");
                    }
                %>
            </thead>
        </table>
        <form action="Loginhandle" method="POST">
             <input type="hidden" name="origin" value="AdminBackToStart" />
             <input type="submit" value="Forside" name="submit" />
        </form>
    </body>
</html>