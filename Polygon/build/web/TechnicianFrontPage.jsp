<%
    if (session.getAttribute("usertype") == null)    {response.sendRedirect("PolygonFrontPage.html");    }
    else if ((int)session.getAttribute("usertype") == 2) {response.sendRedirect("CustomerFrontPage.jsp");}
    else if ((int)session.getAttribute("usertype") == 1) {response.sendRedirect("AdminFrontPage.jsp");}
%>
<%-- 
    Document   : TechnicianFrontPage
    Created on : 03-11-2016, 13:57:12
    Author     : Anders
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Velkommen <%= request.getSession(false).getAttribute("Username")%></title>
    </head>
    <body>
        <h1>Velkommen <%= request.getSession(false).getAttribute("Username")%>. Du er brugertype : <%= request.getSession(false).getAttribute("usertype")%></h1>
        <h2>Fil upload:</h2>
        Vælg upload fil:<br />
        <form action="UploadServlet" method="post" enctype="multipart/form-data">
            <input type="file" name="file" />
            <br />
            <input type="submit" value="Upload File"/>
        </form>
        <form action="Loginhandle" method="POST">
             <input type="hidden" name="origin" value="logout" />
             <input type="submit" value="Log af" name="submit" />
        </form>
    </body>
</html>
