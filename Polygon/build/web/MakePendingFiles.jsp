<%-- 
    Document   : MakePendingFiles
    Created on : Dec 2, 2016, 12:54:01 PM
    Author     : LoD
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form method="POST" action="Byggninghandle">
            <table border="0">
                <thead>
                    <tr>
                        <td>Adresse</td>
                        <th><input type="text" name="address" value="" /></th>
                    </tr>
                    <tr>
                        <td>Post nummer</td>
                        <td><input type="text" name="zip_code" value=""/></td>
                    </tr>
                    <tr>
                        <td>By</td>
                        <td><input type="text" name="city" value="" /></td>
                    </tr>
                    <tr>
                        <td>Hus Nummer</td>
                        <td><input type="text" name="b_number" value="" /></td>
                    </tr>
                    <tr>
                        <td>Etage(r)</td>
                        <td><input type="text" name="floor" /></td>
                    </tr>
                    <tr>
                        <td>M^2</td>
                        <td><input type="text" name="size" /></td>
                    </tr>
                    <tr>
                        <td>Antal rum</td>
                        <td><input type="text" name="number_room" /></td>
                    </tr>
                    <tr>
                        <td>Rum nummer</td>
                        <td><input type="text" name="room_number" value="" /></td>
                    </tr>
                    <tr>
                        <td><input type="radio" name="exet" value="add" />Tilføje<input type="radio" name="exet" value="edit" />Ret<input type="radio" name="exet" value="delete" />Slet</td>
                        <td><input type="submit" value="Send" name="pendingQuest" /></td>
                    </tr>
                </thead>
            </table>
            <input type="hidden" name="type" value="pending" />
        </form>
    </body>
</html>
